package ru.pochta.hooks;

import com.google.inject.Inject;
import io.cucumber.java.After;
import support.GuiceExample;

public class Hooks {
    @Inject
    private GuiceExample guiceExample;

    @After(order = 1)
    public void closeDriver(){
        if (guiceExample.driver !=null)
            guiceExample.driver.quit();
    }
}
