package ru.pochta.steps;

import com.google.inject.Inject;
import io.cucumber.java.ru.Затем;
import io.cucumber.java.ru.И;
import io.cucumber.java.ru.Пусть;
import io.cucumber.java.ru.Тогда;
import org.junit.jupiter.api.Assertions;
import ru.pochta.pages.PochtaMainPage;

public class PochtaMainPageSteps {
    @Inject
    private PochtaMainPage pochtaMainPage;

    @Пусть("Открыта главная страница")
    public void openMainPage() {
        pochtaMainPage.open();
    }

    @Затем("Нажать на ссылку Войти")
    public void clickLoginLink() {
        pochtaMainPage.clickLoginLink();
    }

    @Тогда("Ввести логин {string}")
    public void inputLogin(String text) {
        pochtaMainPage.enterLogin(text);
    }

    @И("Ввести пароль {string}")
    public void inputPass(String text) {
        pochtaMainPage.enterPassword(text);
    }

    @Затем("Нажать кнопку Войти")
    public void clickLoginBut() {
        pochtaMainPage.clickLoginButton();
    }

    @Тогда("Навести на номер")
    public void moveToNumber() {
        pochtaMainPage.moveOver();
    }

    @И("Нажать Мои отправления")
    public void clickMyTracking() {
        pochtaMainPage.clickMyTrackingLink();
    }

    @Тогда("На странице есть текст {string}")
    public void checkText(String text) {
        Assertions.assertEquals(text, pochtaMainPage.getText());
    }

}
