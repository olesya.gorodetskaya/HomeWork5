package ru.pochta.pages;

import com.google.inject.Inject;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import support.GuiceExample;

public class PochtaMainPage extends BasePage<PochtaMainPage> {

    @FindBy(css = "[href=\"/api/auth/login\"]")
    private WebElement loginLink;
    @FindBy(id = "username")
    private WebElement loginField;
    @FindBy(id = "userpassword")
    private WebElement passwordField;
    @FindBy(css = "form > button")
    private WebElement loginButton;
    @FindBy(css = "div[title='79917133197']")
    private WebElement myAccountMenu;
    @FindBy(css = "div > a[href=\"/tracking\"]")
    private WebElement myTrackingLink;
    @FindBy(css = "div > p")
    private WebElement forComparison;

    @Inject
    public PochtaMainPage(GuiceExample guiceExample) {
        super(guiceExample, "");
    }

    public void clickLoginLink() {
        loginLink.click();
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void enterLogin(String text) {
        loginField.sendKeys(text);
    }

    public void enterPassword(String text) {
        passwordField.sendKeys(text);
    }

    public void clickLoginButton() {
        loginButton.click();
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void moveOver() {
        Actions actions = new Actions(guiceExample.driver);  //Instantiate Action Class
        actions.moveToElement(myAccountMenu).perform();    //Mouse hover menuOption
    }

    public void clickMyTrackingLink() {
        myTrackingLink.click();
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public String getText() {
        return forComparison.getText();
    }

}
