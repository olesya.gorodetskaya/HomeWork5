package ru.pochta.pages;

import org.openqa.selenium.support.PageFactory;
import support.GuiceExample;

public abstract class BasePage<T> {
    protected GuiceExample guiceExample;
    protected String path;

    public BasePage(GuiceExample guiceExample,String path){
        PageFactory.initElements(guiceExample.driver, this);
        this.guiceExample=guiceExample;
        this.path=path;
    }

    public T open(){
        guiceExample.driver.get("https://www.pochta.ru/");
        return(T)this;
    }
}
